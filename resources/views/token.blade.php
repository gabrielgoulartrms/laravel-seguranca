@extends('layouts')

@section('content')
<h1 class="text-center my-5">Insira o token enviado por email</h1>

<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card card-default">
      <div class="card-body">
          @if($message ?? '')
          <div class="alert alert-danger" role="alert">
            {{ $message ?? ''}}
          </div>
          @endif 
        <form role="form" method="POST" action="{{ url('/tokenConfirm') }}">
            @csrf
            <div class="form-group">
                <label class="col-md-4 control-label">Token</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="token">
                </div>
                <input type="hidden" class="form-control" name="email" value={{$email}}>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Enviar
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection