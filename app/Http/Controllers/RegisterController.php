<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function token()
    {
        $message = session('message');
        $email = session('email');
        if($message){
            return view('token')->with('message', $message)->with('email', $email);
        }
        return view('token')->with('email', $email);
    }

    public function tokenConfirm(Request $request)
    {
        $userFind = User::where('email', $request->email)->get();
        $user = $userFind[0];
        if($user->token == $request->token){
            $user->verified = true;
            $user->update();
            return redirect('/')->with('user', $user);
        }
        $message = "Token Incorreto!";
        return redirect("/confirm")->with('message', $message)->with('email', $user->email);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password); //criptografia
        $user->verified = false;
        if($request->access == "Adm"){
            $user->level = 0;
        }
        else if($request->access == "Comum"){
            $user->level = 1;
        }
        else if($request->access == "Especial"){
            $user->level = 2;
        }

        $tokenNum = $this->generateToken();
        $user->token = $tokenNum;

        

        $data = $data = array('name' => $request->name, 'token' => $tokenNum);
        $name = $request->name;
        $email = $request->email;
        Mail::send('mail', $data, function($message) use($email, $name) {
            $message->to($email, $name)->subject
            ('Token de acesso');
            $message->from('seggrupo7@gmail.com','Grupo 7');
        });

        $user->save();

        return redirect("/confirm")->with('email', $email);
    }

    public function generateToken($tokenSize = 5){
        $i = 0;
        $token = "";
        while($i < $tokenSize){
            $token .= mt_rand(0, 9);
            $i++;
        }
        return $token;
    }
}
