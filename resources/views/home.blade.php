@extends('layouts')

@section('content')
<h1 class="text-center my-5">Tela Principal</h1>
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card card-default">
      <div class="card-body">
          Olá, {{$user->name}} <br>
          Seu Email: {{$user->email}} <br>
          <p>Você é um usuario do tipo: @if($user->level == 0) Admnistrador @elseif($user->level == 1) Comum @else Especial @endif </p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection