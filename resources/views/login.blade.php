@extends('layouts')

@section('content')
<h1 class="text-center my-5">Login</h1>

<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card card-default">
      <div class="card-body">
          @if($message ?? '')
          <div class="alert alert-danger" role="alert">
            {{ $message ?? ''}}
          </div>
          @endif 
        <form role="form" method="POST" action="{{ url('/loguser') }}">
            @csrf

            <div class="form-group">
                <label class="col-md-4 control-label">E-Mail</label>

                <div class="col-md-9">
                    <input type="email" class="form-control" name="email">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Senha</label>

                <div class="col-md-9">
                    <input type="password" class="form-control" name="password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Login
                    </button>
                </div>
            </div>
        </form>
        <div class="col-md-6 col-md-offset-4 mt-4">
            <a href="/register" type="submit" class="btn btn-secondary">
                Registrar
            </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection