@extends('layouts')

@section('content')
<h1 class="text-center my-5">Login</h1>

<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card card-default">
      <div class="card-body">
        <form role="form" method="POST" action="{{ url('/registerUser') }}">
            @csrf

            <div class="form-group">
                <label class="col-md-4 control-label">Nome</label>

                <div class="col-md-9">
                    <input type="text" class="form-control" name="name" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">E-Mail</label>

                <div class="col-md-9">
                    <input type="email" class="form-control" name="email" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Senha</label>

                <div class="col-md-9">
                    <input type="password" class="form-control" name="password" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Sua senha precisa conter 1 letra maiuscula, 1 letra minúscula, 1 número e ao menos 8 caracteres" required>
                </div>
                <br>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="bar"></div>
                </div>
            </div>

            <div class="form-group">
                <label for="accessLevel">Níveis de acesso</label>
                <select class="form-control" name="access" id="accessLevel">
                  <option>Adm</option>
                  <option>Comum</option>
                  <option>Especial</option>
                </select>
              </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-btn fa-sign-in"></i>Criar
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){

        $("#password").keyup(function() {
            
            var strength = 0;

            /*length 5 characters or more*/
            if(this.value.length >= 8) {
                strength++;
            }

            /*contains lowercase characters*/
            if(this.value.match(/[a-z]+/) && this.value.length >= 9) {
                strength++;
            }

            /*contains digits*/
            if(this.value.match(/[0-9]+/) && this.value.length >= 9) {
                strength++;
            }

            /*contains uppercase characters*/
            if(this.value.match(/[A-Z]+/) && this.value.length >= 9) {
                strength++;
            }

            bar = $("#bar");
            if(strength == 0){
                bar.css('width', "0%");
            }
            if(strength == 1){
                bar.html("Fraca");
                bar.css('width', "25%");
            }
            else if(strength == 2){
                bar.html("Média");
                bar.css('width', "50%");
            }
            else if(strength === 3){
                bar.html("Forte");
                bar.css('width', "75%");
            }
            else if(strength == 4){
                bar.html("Super Forte");
                bar.css('width', "100%");
            }

        });
    });
</script>
@endsection